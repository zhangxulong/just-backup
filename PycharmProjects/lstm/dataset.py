import cPickle
import os
import os.path
from time import time
import wave
import scipy
from numpy import *
import numpy
import scipy.io.wavfile
import pylab
from scikits.talkbox.features import mfcc
import math

__author__ = 'zhangxulong'


def draw_wav(wav_dir):
    print "begin draw_wav ==feature_extraction.py=="
    song = wave.open(wav_dir, "rb")
    params = song.getparams()
    nchannels, samplewidth, framerate, nframes = params[:4]  # format info
    song_data = song.readframes(nframes)
    song.close()
    wave_data = numpy.fromstring(song_data, dtype=numpy.short)
    wave_data.shape = -1, 1
    wave_data = wave_data.T
    time = numpy.arange(0, nframes) * (1.0 / framerate)
    len_time = len(time)
    time = time[0:len_time]

    pylab.plot(time, wave_data[0])

    pylab.xlabel("time")
    pylab.ylabel("wav_data")
    pylab.show()
    return 0


def get_mfcc(wav_dir):
    sample_rate, audio = scipy.io.wavfile.read(wav_dir)
    ceps, mspec, spec = mfcc(audio, nwin=256, fs=8000, nceps=13)
    mfccs = ceps
    return mfccs


def get_pca(mata, length):
    # PCA reduce the dimension
    meanVal = mean(mata, axis=0)
    stdVal = std(mata)
    rmmeanMat = (mata - meanVal) / stdVal
    covMat = cov(rmmeanMat, rowvar=0)
    eigval, eigvec = linalg.eig(covMat)
    maxnum = argsort(-eigval, axis=0)  # sort descend
    tfMat = eigvec[:, maxnum[0:length]]  # top length
    finalData = dot(rmmeanMat, tfMat)  #
    recoMat = finalData * tfMat.T * stdVal + meanVal
    return finalData, recoMat


def pickle_dataset(dataset):
    pkl_file = file('dataset.pkl', 'wb')
    cPickle.dump(dataset, pkl_file, True)
    pkl_file.close()
    return 0


def filter_nan_inf(mfccss):
    filter_nan_infs = []
    for item in mfccss:
        new_item = numpy.nan_to_num(item)
        filter_nan_infs.append(new_item)
    new_mfcc = numpy.array(filter_nan_infs)
    return new_mfcc


def mfcc_sum_vector(song_feature):
    sums = []
    for mfcc_item in song_feature:
        sums.append(sum(mfcc_item))
    return sums


def build_dataset(datasets_dir):
    print 'from %s build dataset==============' % datasets_dir
    dataset = []
    data = []
    target = []
    dict_singer_label = {"aerosmith": 0, "beatles": 1, "creedence_clearwater_revival": 2, "cure": 3,
                         "dave_matthews_band": 4, "depeche_mode": 5, "fleetwood_mac": 6, "garth_brooks": 7,
                         "green_day": 8, "led_zeppelin": 9, "madonna": 10, "metallica": 11, "prince": 12, "queen": 13,
                         "radiohead": 14, "roxette": 15, "steely_dan": 16, "suzanne_vega": 17, "tori_amos": 18,
                         "u2": 19}

    for parent, dirnames, filenames in os.walk(datasets_dir):
        for filename in filenames:
            song_dir = os.path.join(parent, filename)
            print"get mfcc of %s ====" % filename
            song_feature = get_mfcc(song_dir)
            mfcc_file = open('mfcc.txt', 'a')
            mfcc_file.write(song_dir)
            mfcc_file.write(str(song_feature) + '\r\n')
            print 'filter mfcc nan inf'
            song_feature = filter_nan_inf(song_feature)  # feature=======================a song mfcc vector
            fprint = open('mfcc_feature.txt', 'a')
            fprint.write(str(song_feature) + '\r\n')
            fprint.close()
            singer = song_dir.split('/')[1]  # this value depends on the singer file level in the dir
            singer_label = dict_singer_label[singer]  # target class====================
            # song_mfcc_sum_vector = mfcc_sum_vector(song_feature)
            #  feature=======================a song mfcc vector sum
            for vector_item in song_feature:
                data.append(vector_item)  # feature just a frame
                target.append(singer_label)
    dataset.append(data)
    dataset.append(target)
    print 'pkl_to dataset.pkl'
    pickle_dataset(dataset)
    return 0


def load_dataset(pkl_dir):
    pkl_file = file(pkl_dir, 'rb')
    dataset = cPickle.load(pkl_file)
    pkl_file.close()
    return dataset


def split_dataset(dataset):
    data_x = dataset['data']
    target_y = dataset['singers_label']
    dict_singer_label = dataset['dict_singer_label']
    total_len = len(data_x)
    train_par = 1
    train_data_x = data_x[0:train_par * total_len]
    train_target_y = target_y[0:train_par * total_len]
    test_data_x = data_x[train_par * total_len:total_len]
    test_target_y = target_y[train_par * total_len:total_len]
    trainset = {}
    testset = {}
    trainset['data'] = train_data_x
    trainset['target'] = train_target_y
    trainset['dict'] = dict_singer_label
    testset['data'] = test_data_x
    testset['target'] = test_target_y
    testset['dict'] = dict_singer_label
    return trainset, testset


def test(dirss):
    return 0


if __name__ == '__main__':
    print 'start===================================='
    start = time()
    build_dataset('dataset_E_vocal')

    finish = time()
    print"build dataset takes %.2f" % (finish - start)
    timefile = open('time.txt', 'a')

    timefile.write(str(finish - start) + '\r\n')
