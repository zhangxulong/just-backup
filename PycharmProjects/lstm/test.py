__author__ = 'zhangxulong'
from  dataset import get_mfcc
import matplotlib.pyplot as plt


def plot_sums(mfccs):
    sums = []
    for mfcc_item in mfccs:
        sums.append(sum(mfcc_item))
    x = [item for item in range(len(sums))]
    plt.plot(x, sums)
    plt.xlabel('x')
    plt.ylabel('mfcc sums')
    plt.title('mfcc in time series')
    plt.legend()
    plt.show()
    return 0


mfccs = get_mfcc('01-Make_It_E.wav')
print(mfccs.shape)
#plot_sums(mfccs)
mfccs_sum_vector=[]
for item in mfccs:
    mfccs_sum_vector.append(sum(item))
print len(mfccs_sum_vector)
